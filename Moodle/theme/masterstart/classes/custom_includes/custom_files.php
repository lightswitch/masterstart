<?php
public function johnos_test() {
        global $CFG;
        global $DB;

        $sql = "SELECT *
        FROM mdl_course WHERE category = 1";

        $rs = $DB->get_recordset_sql($sql);
        $menu_items = '';
        foreach($rs as $r){
            $menu_items.= '<a href="http://moodle.dev/course/view.php?id=' . $r->id . '"><span style="color:red;">' . $r->fullname . '</span></a>';
        }

        return $menu_items; // . 'theme\boost\classes\output\core_renderer/php  use this to generate the menu items';
    }

    public function johno_content() {
        global $CFG;
        global $DB;
        global $USER;

        if(isset($_GET['id'])){
            $AND = ' AND mdl_course.id = ' . $_GET['id'];
        } else {
            $AND = '';
        }
        $course_sql = "SELECT 
        mdl_course.id as course_id,
        mdl_course.fullname as course_name,
        mdl_user.id as user_id
        FROM mdl_course
        JOIN mdl_context  ON mdl_course.id = mdl_context.instanceid
        JOIN mdl_role_assignments ON mdl_role_assignments.contextid = mdl_context.id
        JOIN mdl_user ON mdl_user.id = mdl_role_assignments.userid
        JOIN mdl_role ON mdl_role.id = mdl_role_assignments.roleid
        WHERE mdl_user.id = " . $USER->id . $AND;

        $enrolled_records = $DB->get_recordset_sql($course_sql);

        if(isset($_GET['id'])){
            $module_sql = "SELECT id FROM mdl_course_sections WHERE course = " . $_GET['id'];
            $module_results = $DB->get_recordset_sql($module_sql);
        } else {
            $module_results = array();
        }

        $block_render = '';
        foreach($enrolled_records as $enrolled_record){
            //GET THE MODULES AND COMPLETIONS
            $block_render.= '<div style="border:1px solid red;">
                               <h2>' . $enrolled_record->course_name . '</h2>
                               <br />';
                            //DO THE BLOCK MODULE/COUNT
                            $count = 0;                               
                            foreach($module_results as $module_result){
                                $block_render.= '<span style="padding:5px; background-color:#93b5ea; border:1px solid black">' . $count++ . '</span>';
                            }
                            $block_render.= '<br />
                                </div>';
        }

        $block_render.= '<br /><br />';

        return $block_render;
    }