<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_masterstart\output;

use coding_exception;
use html_writer;
use tabobject;
use tabtree;
use custom_menu_item;
use custom_menu;
use block_contents;
use navigation_node;
use action_link;
use stdClass;
use moodle_url;
use preferences_groups;
use action_menu;
use help_icon;
use single_button;
use single_select;
use paging_bar;
use url_select;
use context_course;
use pix_icon;

defined('MOODLE_INTERNAL') || die;

/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 * @package    theme_masterstart
 * @copyright  2012 Bas Brands, www.basbrands.nl
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class core_renderer extends \theme_boost\output\core_renderer {

    /** @var custom_menu_item language The language menu if created */
    protected $language = null;

    /**
     * Outputs the opening section of a box.
     *
     * @param string $classes A space-separated list of CSS classes
     * @param string $id An optional ID
     * @param array $attributes An array of other attributes to give the box.
     * @return string the HTML to output.
     */
    public function box_start($classes = 'generalbox', $id = null, $attributes = array()) {
        if (is_array($classes)) {
            $classes = implode(' ', $classes);
        }
        return parent::box_start($classes . ' p-y-1', $id, $attributes);
    }

    /**
     * Wrapper for header elements.
     *
     * @return string HTML to display the main header.
     */
    public function full_header() {
        global $PAGE;

        $html = html_writer::start_tag('header', array('id' => 'page-header', 'class' => 'row'));
        $html .= html_writer::start_div('col-xs-12 p-a-1');
        $html .= html_writer::start_div('card');
        $html .= html_writer::start_div('card-block');
        $html .= html_writer::div($this->context_header_settings_menu(), 'pull-xs-right context-header-settings-menu');
        $html .= html_writer::start_div('pull-xs-left');
        $html .= $this->context_header();
        $html .= html_writer::end_div();
        $pageheadingbutton = $this->page_heading_button();
        if (empty($PAGE->layout_options['nonavbar'])) {
            $html .= html_writer::start_div('clearfix w-100 pull-xs-left', array('id' => 'page-navbar'));
            $html .= html_writer::tag('div', $this->navbar(), array('class' => 'breadcrumb-nav'));
            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button pull-xs-right');
            $html .= html_writer::end_div();
        } else if ($pageheadingbutton) {
            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button nonavbar pull-xs-right');
        }
        $html .= html_writer::tag('div', $this->course_header(), array('id' => 'course-header'));
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_tag('header');
        return $html;
    }

    /**
     * The standard tags that should be included in the <head> tag
     * including a meta description for the front page
     *
     * @return string HTML fragment.
     */
    public function standard_head_html() {
        global $SITE, $PAGE;

        $output = parent::standard_head_html();
        if ($PAGE->pagelayout == 'frontpage') {
            $summary = s(strip_tags(format_text($SITE->summary, FORMAT_HTML)));
            if (!empty($summary)) {
                $output .= "<meta name=\"description\" content=\"$summary\" />\n";
            }
        }

        return $output;
    }

    /*
     * This renders the navbar.
     * Uses bootstrap compatible html.
     */
    public function navbar() {
        return $this->render_from_template('core/navbar', $this->page->navbar);
    }

    /**
     * We don't like these...
     *
     */
    public function edit_button(moodle_url $url) {
        return '';
    }

    /**
     * Override to inject the logo.
     *
     * @param array $headerinfo The header info.
     * @param int $headinglevel What level the 'h' tag will be.
     * @return string HTML for the header bar.
     */
    public function context_header($headerinfo = null, $headinglevel = 1) {
        global $SITE;

        if ($this->should_display_main_logo($headinglevel)) {
            $sitename = format_string($SITE->fullname, true, array('context' => context_course::instance(SITEID)));
            return html_writer::div(html_writer::empty_tag('img', [
                'src' => $this->get_logo_url(null, 150), 'alt' => $sitename]), 'logo');
        }

        return parent::context_header($headerinfo, $headinglevel);
    }

    /**
     * Get the compact logo URL.
     *
     * @return string
     */
    public function get_compact_logo_url($maxwidth = 100, $maxheight = 100) {
        return parent::get_compact_logo_url(null, 70);
    }

    /**
     * Whether we should display the main logo.
     *
     * @return bool
     */
    public function should_display_main_logo($headinglevel = 1) {
        global $PAGE;

        // Only render the logo if we're on the front page or login page and the we have a logo.
        $logo = $this->get_logo_url();
        if ($headinglevel == 1 && !empty($logo)) {
            if ($PAGE->pagelayout == 'frontpage' || $PAGE->pagelayout == 'login') {
                return true;
            }
        }

        return false;
    }
    /**
     * Whether we should display the logo in the navbar.
     *
     * We will when there are no main logos, and we have compact logo.
     *
     * @return bool
     */
    public function should_display_navbar_logo() {
        $logo = $this->get_compact_logo_url();
        return !empty($logo) && !$this->should_display_main_logo();
    }

    /*
     * Overriding the custom_menu function ensures the custom menu is
     * always shown, even if no menu items are configured in the global
     * theme settings page.
     */
    public function custom_menu($custommenuitems = '') {
        global $CFG;

        if (empty($custommenuitems) && !empty($CFG->custommenuitems)) {
            $custommenuitems = $CFG->custommenuitems;
        }
        $custommenu = new custom_menu($custommenuitems, current_language());
        return $this->render_custom_menu($custommenu);
    }

    /*
     * Create the custom menu
     * Edit the theme to allow for drop down menus
    */

    public function user_profile_pic() {
      global $USER;

      return '/user/pix.php/' . $USER->id . '/f1.jpg';
    }

    public function version() {
      $version = '1';
      return '?v=' . $version;
    }

    public function breadcrumb() {
        global $CFG;
        global $DB;
        global $USER;
        global $PAGE;
        global $OUTPUT;
        global $COURSE;

        $actual_link = $_SERVER[REQUEST_URI];

        $output = '<li class="breadcrumb-item"><a href="' . $CFG->wwwroot . '/my/">Dashboard</a></li>';
  
        if ($actual_link == '/my/?page=assignment') {
          $output.= '<li class="breadcrumb-item"><a href="' . $CFG->wwwroot . '/my/?page=assignment">Assignments</a></li>';
        } elseif ($actual_link == '/calendar/view.php') {
          $output.= '<li class="breadcrumb-item"><a href="' . $CFG->wwwroot . '/calendar/view.php">Calendar</a></li>';
        } else {
          $bread_crumbs_array = explode('</li>', $OUTPUT->navbar());

          if ($COURSE->id != 1) {
            if(!strpos($_SERVER[REQUEST_URI],"mod")) {
              $course_name = $DB->get_record('course', ['id' => $COURSE->id], 'id, fullname', IGNORE_MISSING);
              $output.= '<li class="breadcrumb-item"><a href="' . $CFG->wwwroot . '/course/view.php?id=' . $course_name->id . '">' . $course_name->fullname . '</a></li>';
            }
          }
  
          if($_GET['section'] != '') {
            $module_number = $_GET['section'] - 1;
            $output.= '<li class="breadcrumb-item"><a href="' . $CFG->wwwroot . '/course/view.php?id=' . $COURSE->id . '&section=' . $_GET['section'] . '">Module ' . $module_number  . '</a></li>';
          }
          
          if(strpos($_SERVER[REQUEST_URI],"mod")) {
            $course_name = $DB->get_record('course', ['shortname' => ltrim(strip_tags($bread_crumbs_array[2]))], 'id, fullname', IGNORE_MISSING);
            $output.= '<li class="breadcrumb-item"><a href="' . $CFG->wwwroot . '/course/view.php?id=' . $course_name->id . '">' . $course_name->fullname . '</a></li>';
            $output.= $bread_crumbs_array[3];
            $output.= $bread_crumbs_array[4];
          }
        }
         
        $output.= '</ol>'; 
         
        return $output;
    }

    public function master_start_course_menu() {
      global $CFG;
      global $DB;
      global $USER;

      $courses = enrol_get_users_courses($USER->id, true);
      if (count($courses) > 0) {

        $menu_items = array();

        foreach($courses as $course) {

          $modinfo = get_fast_modinfo($course->id);
          $link_array = array();
          $activities = get_array_of_activities($course->id);
          $menu_items[$course->id]->details = $course;
 
          $total = 0;
          $completed = 0;
          foreach($activities as $activity) {
            if ($activity->visible != 0 && $activity->deletioninprogress != 1) {
           
              if ($activity->section != 0) {
                $cm = $modinfo->get_cm($activity->cm);

                if(!$cm->uservisible) {
                  continue;
                }
                $menu_items[$course->id]->modules[$activity->section]->activities[] = $activity;
                if($activity->completion >= 1){
                  $menu_items[$course->id]->modules[$activity->section]->completed++;
                }
                $menu_items[$course->id]->modules[$activity->section]->total++;
                $menu_items[$course->id]->modules[$activity->section]->section = $activity->section;
              }
            }
          }
        }

        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);

        $active_dashboard = '';
        $active_course = '';
        $active_assignments = '';
        $active_calendar = '';

        if ($uri_segments[1] == 'my') {
          if ($uri_segments[1] == 'my' && $_GET['page'] == 'assignment') {
            $active_assignments = 'active';
          } else {
            $active_dashboard = 'active';
          }
        }

        if ($uri_segments[1] == 'course' || $uri_segments[1] == 'mod') {
          $active_course = 'active';
        }

        if ($uri_segments[1] == 'calendar') {
          $active_calendar = 'active';
        }

        $menu_output = '
           <li class="nav-item ' . $active_dashboard . '">
            <a class="nav-link" href="/">Dashboard</a>
          </li>';

        $menu_output.= '
          <li class="nav-item dropdown ' . $active_course . '">
            <span class="nav-link dropdown-toggle" href="#" id="courseDropdown" role="button" data-toggle="dropdown" data-target="#">
              My Courses
            </span>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">';

       foreach($menu_items as $menu_item) {
          $menu_output.= '
            <li class="dropdown-submenu">
              <a class="dropdown-item" href="' . $CFG->wwwroot . '/course/view.php?id=' . $menu_item->details->id . '">' . substr($menu_item->details->fullname, 0, 30) . '...</a>';

          if ($menu_item->modules) {
            $menu_item->modules = array_reverse($menu_item->modules);
            $menu_output.= '<ul class="dropdown-menu">';
            foreach($menu_item->modules as $module) {

              if ($module->total == $module->completed) {
                $completed = 'completed';
              } else {
                $completed = '';
              }

              //get module availability
              $module_array = array();
              $module_sql = "SELECT id, name, availability, section, visible
                                           FROM " . $CFG->prefix . "course_sections 
                                           WHERE name IS NOT NULL 
                                           AND course = " . $menu_item->details->id . ' 
                                           AND section = ' . $module->section;

              $module_results = $DB->get_recordset_sql($module_sql);
              $count = 0;
              foreach($module_results as $module_result){

                if ($module_result->section == 0) {
                  continue;
                }
                $range_arrays = array( json_decode($module_result->availability, true));
                //Assign variables to the dates to make them human readable for the comparisons
                $module_array[$module->section] = date('Y-m-d', $range_arrays[0]['c'][0]['t']);
                //$module_start = date('Y-m-d', $range_arrays[0]['c'][0]['t']);
              }
              //end module availabilty

              if(strtotime(date('d M Y')) < strtotime($module_array[$module->section])) {
                $menu_output.= '<li>
                        <span class="dropdown-item module locked">
                          <div class="course-name">Module ' . ($module->section - 1) . '</div>
                          <div class="completion-icon ' . $completed . '"></div>
                        </span>';
              } else {
                $menu_output.= '<li class="dropdown-submenu">
                        <a class="dropdown-item module" href="' . $CFG->wwwroot . '/course/view.php?id=' . $menu_item->details->id . '&section=' . $module->section . '">
                          <div class="course-name">Module ' . ($module->section - 1) . '</div>
                          <div class="completion-icon ' . $completed . '"></div>
                        </a>';
              }
              
              $menu_output .= '<ul class="dropdown-menu">';
              foreach($module->activities as $activity) {
                if($activity->mod == 'scorm') {
                  continue;
                }
                if ($activity->mod != 'label') {
                  $menu_output.= '
                    <li>
                      <a class="dropdown-item" href="' . $CFG->wwwroot . '/mod/' . $activity->mod . '/view.php?id=' . $activity->cm . '">
                        <div class="course-icon ';
                        switch($activity->mod) {
                          case 'assign':
                            $menu_output.= 'quiz';
                            break;
                          case 'forum':
                            $menu_output.= 'forum';
                            break;
                          case 'page':
                            $menu_output.= 'book';
                            break;
                          case 'quiz':
                            $menu_output.= 'quiz';
                            break;
                          case 'book':
                            $menu_output.= 'book';
                            break;
                          case 'scorm':
                            $menu_output.= 'module';
                            break;
                          default:
                            $menu_output.= 'book';
                        }

                        if($activity->completion == 2){
                          $completed = 'completed';
                        } else {
                          $completed = '';
                        }

                        $menu_output.= '"></div>
                        <div class="course-name">' . substr($activity->name, 0, 30) . '</div>
                        <div class="completion-icon ' . $completed . '"></div>
                      </a>
                    </li>';
                }  else {
                  $menu_output.= '
                    <li>
                      <a class="dropdown-item remove-caret">
                        <div class="course-name">' . substr($activity->name, 0, 30) . '</div>
                      </a>
                    </li>';
                }
              }
              $menu_output .= '</li></ul>';
            }
            $menu_output.= '</li></ul>';
          }
        }
        $menu_output.= '</li></ul>';
      }

      $menu_output .= '
        <li class="nav-item ' . $active_assignments . '">
          <a class="nav-link" href="/my/?page=assignment">My Assignments</a>
        </li>
        <li class="nav-item ' . $active_calendar . '">
          <a class="nav-link" href="/calendar">Calendar</a>
        </li>
      ';
      return $menu_output;
    }

    public function master_start_blocks() {
        global $CFG;
        global $DB;
        global $USER;
        global $OUTPUT;

        $range_array = array();

        if($USER->id != 0) {
            $courses = enrol_get_users_courses($USER->id, true);

            //Assign variables to the dates to make them human readable for the comparisons
            date_default_timezone_set('Africa/Johannesburg');
            $today = date('Y-m-d', strtotime(date('Y-m-d')));
            $current_start = 0;
            $current_end = 0;

            foreach($courses as $course) {

              $modinfo = get_fast_modinfo($course->id);
              $link_array = array();
              $activities = get_array_of_activities($course->id);
              
              $sections = array();
              $total = 0;
              $completed = 0;
              $visible = 0;
              foreach($activities as $activity){
                if ($activity->section != 0) {
                  $cm = $modinfo->get_cm($activity->cm);
                  $section = $modinfo->get_section_info($activity->section);

                  $sections[$activity->section]->availability = $modinfo->get_section_info_all()[$section->section]->availability;
                  $sections[$activity->section]->name = $section->name;
                  $sections[$activity->section]->course = $section->course;
                  $sections[$activity->section]->id = $section->section;
                  $sections[$activity->section]->visible = $section->visible;
                  $sections[$activity->section]->activities[] = $activity;
                  if($activity->completion == 2){
                    $sections[$activity->section]->completed++;
                  }
                  $sections[$activity->section]->total++;
                }
              }
              
              $section_complete = array();    
              foreach($sections as $section) {
                if ($section->total != 0 || $section->completed != 0) {
                  $section_complete[$section->name] = number_format((($section->completed / $section->total) * 100), 0);
                } else {
                   $section_complete[$section->name] = 0;
                }
              }

              //GET THE MODULES AND COMPLETIONS
              $block_render.= '<div class="card">
                                <div class="card-body">
                                  <h4 class="card-title">My Course</h4>
                                  <p class="card-text">' . $course->fullname . '</p>
                                  <h6>';

              $module_sql = "SELECT id, name, availability, section, visible
                             FROM " . $CFG->prefix . "course_sections 
                             WHERE name IS NOT NULL 
                             AND visible = 1
                             AND course = " . $course->id;
              $module_results = $DB->get_recordset_sql($module_sql);

              // HAVE TO DO A LOOP HERE TO GET THE MODULE TITLE -> IF MORE THAN 1 TITLE IS AVAILABLE IT WILL BREAK
              foreach($module_results as $module_result){

                $range_arrays = array( json_decode($module_result->availability, true));
                $module_start = date('Y-m-d', $range_arrays[0]['c'][0]['t']);
                $module_end = date('Y-m-d', $range_arrays[0]['c'][1]['t']);
            
                if(($today >= $module_start) && ($today < $module_end)) {
                  $block_render.= $module_result->name;
                }
              }

              $block_render .= '</h6>';

              $module_sql = "SELECT id, name, availability, section, visible
                             FROM " . $CFG->prefix . "course_sections 
                             WHERE name IS NOT NULL 
                             AND course = " . $course->id;

              $module_results = $DB->get_recordset_sql($module_sql);

              $block_render .= '<div class="course-progress-buttons">';

              //DO THE BLOCK MODULE/COUNT
              $count = 0;
              foreach($module_results as $module_result){

                if ($module_result->section == 0) {
                  continue;
                }

                $range_arrays = array( json_decode($module_result->availability, true));
                //Assign variables to the dates to make them human readable for the comparisons
                $module_start = date('Y-m-d', $range_arrays[0]['c'][0]['t']);
                if (isset($range_arrays[0]['c'][1]['t'])) {
                  $module_end = date('Y-m-d', $range_arrays[0]['c'][1]['t']);
                } else {
                  $module_end = strtotime($module_start);
                  $module_end = strtotime('+7 day', $module_end);
                  $module_end = date('Y-m-d', $module_end);
                }
                
                switch ($today) {
                    case ($today < $module_start):
                         $block_render.= '<div class="course-button locked"><img src="/theme/masterstart/images/lock.svg"></div>';
                        break;

                    case ($today >= $module_start && $section_complete[$module_result->name] != 100):
                        if($today >= $module_start && $today < $module_end) {
                          $current_start = strtotime($module_start);
                          $current_end = strtotime($module_end);
                          $block_render .= '<div class="course-button current"><a href="' . $CFG->wwwroot . '/course/view.php?id='.$course->id  .'&section=' . $module_result->section . '">' . $count . '</a></div>';
                        } else {
                         $block_render .= '<div class="course-button active">
                         <a href="' . $CFG->wwwroot . '/course/view.php?id='.$course->id  .'&section=' . $module_result->section . '">' . $count . '</a></div>';
                        }
                        break;

                    case ($today >= $module_start && $section_complete[$module_result->name] == 100):
                        if($today >= $module_start && $today < $module_end) {
                            $current_start = strtotime($module_start);
                            $current_end = strtotime($module_end);
                            $block_render .= '<div class="course-button complete current">
                            <a href="' . $CFG->wwwroot . '/course/view.php?id='.$course->id  .'&section=' . $module_result->section . '">&nbsp;&nbsp;</a></div>';
                            break;
                         } else {
                            $block_render .= '<div class="course-button complete">
                            <a href="' . $CFG->wwwroot . '/course/view.php?id='.$course->id  .'&section=' . $module_result->section . '">&nbsp;&nbsp;</a></div>';
                            break;
                         }
                  }
                  $count++;
                }

                $block_render .= '</div>
                                  <p class="card-text module-dates">
                                    This module’s start and end dates: <br/>
                                    <span class="clock-icon">
                                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 96"><path d="M48 4C23.7 4 4 23.7 4 48c0 24.3 19.7 44 44 44 24.3 0 44-19.7 44-44C92 23.7 72.3 4 48 4zM48 84c-19.9 0-36-16.1-36-36s16.1-36 36-36 36 16.1 36 36S67.9 84 48 84z"/><path d="M52 46.3V24c0-2.2-1.8-4-4-4s-4 1.8-4 4v24c0 0 0 0 0 0 0 0.3 0 0.5 0.1 0.8 0 0.1 0.1 0.2 0.1 0.4 0 0.1 0.1 0.3 0.1 0.4 0.1 0.1 0.1 0.3 0.2 0.4 0.1 0.1 0.1 0.2 0.2 0.3 0.1 0.2 0.3 0.4 0.5 0.6l11.3 11.3c1.6 1.6 4.1 1.6 5.7 0 1.6-1.6 1.6-4.1 0-5.7L52 46.3z"/></svg>
                                    </span>';
                if(gmdate('d M Y', $current_start) == '01 Jan 1970') {
                  $block_render .= 'There are currently no dates';
                } else {
                   $block_render .= gmdate('d M Y', $current_start) . ' to ' . gmdate('d M Y', $current_end);
                }
                $block_render .= '</p>  
                                </div>';

                $block_render .= '<div class="card-footer">
                                    <a href="' . $CFG->wwwroot . '/course/view.php?id=' . $course->id . '" class="btn btn-secondary btn-block">Go to course</a>
                                  </div>';

                $block_render .= '</div>';
            }
        } else {
            $block_render = 'Please login';
        }
        return $block_render;
    }

    public function baseurl() {
      global $CFG;
      return $CFG->wwwroot;
    }

    public function master_start_assignment_block() {
        global $CFG;
        global $DB;
        global $USER;

        $enrolled_course_sql = "SELECT 
            " . $CFG->prefix . "course.id as course_id,
            " . $CFG->prefix . "course.fullname as course_name,
            " . $CFG->prefix . "user.id as user_id
            FROM " . $CFG->prefix . "course
            JOIN " . $CFG->prefix . "context ON " . $CFG->prefix . "course.id = " . $CFG->prefix . "context.instanceid
            JOIN " . $CFG->prefix . "role_assignments ON " . $CFG->prefix . "role_assignments.contextid = " . $CFG->prefix . "context.id
            JOIN " . $CFG->prefix . "user ON " . $CFG->prefix . "user.id = " . $CFG->prefix . "role_assignments.userid
            JOIN " . $CFG->prefix . "role ON " . $CFG->prefix . "role.id = " . $CFG->prefix . "role_assignments.roleid
            WHERE " . $CFG->prefix . "user.id = " . $USER->id;

        $enrolled_course_results = $DB->get_recordset_sql($enrolled_course_sql);

        //LOOP THROUGH $enrolled_course_results AND ADD THE COURSE ID'S TO AN ARRAY SO THAT WE CAN CHECK USER ENROLLMENTS LOWER IN THE SCRIPT
        //MIGHT BE BENEFICIAL TO ABSTRACT THIS FUNCTION AS IT MAY BE USED ELSEWHERE
        $enrolled_ids = array();
        foreach($enrolled_course_results as $enrolled_course_result){
          $enrolled_ids[] = $enrolled_course_result->course_id;
        }
        $enrolled_ids = implode(",",$enrolled_ids);
        $module_name_sql = "SELECT 
          " . $CFG->prefix . "assign.id,
          " . $CFG->prefix . "assign.name as assignment_name,
          " . $CFG->prefix . "assign.duedate,
          " . $CFG->prefix . "course_sections.section,
          " . $CFG->prefix . "assign.course,
          " . $CFG->prefix . "course_sections.name,
          " . $CFG->prefix . "course_sections.availability
          FROM " . $CFG->prefix . "assign
          INNER JOIN " . $CFG->prefix . "course_sections ON " . $CFG->prefix . "assign.course = " . $CFG->prefix . "course_sections.section
          WHERE " . $CFG->prefix . "assign.course IN ('" . $enrolled_ids . "')
          GROUP BY " . $CFG->prefix . "assign.id
          ORDER BY " . $CFG->prefix . "assign.duedate ASC
          LIMIT 3";

        $module_name_results = $DB->get_recordset_sql($module_name_sql);
        $assignment_block = '<ul class="assignment-list">';

        foreach($module_name_results as $module_name_result){

          $link_array=array();
          $activities = get_array_of_activities($module_name_result->course);
          foreach($activities as $activity) {
            if($activity->mod == 'assign'){
              $link_array[$activity->name] = $activity->cm;
            }
          }

          $availabilty_array = array( json_decode($module_name_result->availability, true));

          //NEED TO CHECK IF THE USER HAS SUBMITTED SOMETHING HERE
          $assignment_uploaded_sql = "SELECT
            " . $CFG->prefix . "assign_submission.id,
            " . $CFG->prefix . "assign_submission.timecreated,
            " . $CFG->prefix . "assign_submission.status 
            FROM " . $CFG->prefix . "assign_submission
            WHERE " . $CFG->prefix . "assign_submission.userid = " . $USER->id . " AND " . $CFG->prefix . "assign_submission.assignment = " . $module_name_result->id . "
            AND " . $CFG->prefix . "assign_submission.status = 'submitted'";

          $assignment_upload_results = $DB->get_record_sql($assignment_uploaded_sql);

          if($assignment_upload_results->status == 'submitted'){
            $assignment_block.= '
              <li class="assignment-list-item complete">
                <a href="' . $CFG->wwwroot . '/mod/assign/view.php?id='. $link_array[$module_name_result->assignment_name] .'">
                  <h6 class="assignment-heading">' . $module_name_result->assignment_name . '</h6>
                    <p class="assignment-upload-date">Uploaded on ' . gmdate('d M h:i A', $assignment_upload_results->timecreated) . '</p>
                </a>
              </li>';
            } else {
              $assignment_block.=  '
                <li class="assignment-list-item">
                  <a href="' . $CFG->wwwroot . '/mod/assign/view.php?id='. $link_array[$module_name_result->assignment_name] .'">
                    <h6 class="assignment-heading">' . $module_name_result->assignment_name . '</h6>
                     <p class="assignment-upload-date">Upload by ' . date('d M h:i A', $module_name_result->duedate) . '</p>
                  </a>
                </li>';
            }
        }

        $assignment_block .= '</ul>';
        return $assignment_block;
    }

    public function master_start_next_assignment_due() {
      global $CFG;
      global $DB;
      global $USER;
      $courses = enrol_get_users_courses($USER->id, true);

      foreach($courses as $course) {
        $course_assignments_sql = "SELECT
                " . $CFG->prefix . "assign.id,
                " . $CFG->prefix . "assign.name as assignment_name,
                " . $CFG->prefix . "assign.duedate,
                " . $CFG->prefix . "course_sections.section,
                " . $CFG->prefix . "course_sections.name as module_name,
                " . $CFG->prefix . "course_sections.availability
                FROM " . $CFG->prefix . "assign
                INNER JOIN " . $CFG->prefix . "course_sections ON " . $CFG->prefix . "assign.course = " . $CFG->prefix . "course_sections.section
                WHERE " . $CFG->prefix . "assign.course = " . $course->id . "
                AND " . $CFG->prefix . "assign.duedate > DATE(NOW())
               GROUP BY " . $CFG->prefix . "assign.id ORDER BY " . $CFG->prefix . "assign.duedate ASC LIMIT 1 ";

        $course_assignments = $DB->get_recordset_sql($course_assignments_sql);

        foreach($course_assignments as $course_assignment) {
          if($course_assignment->duedate != '') {
            return 'Next assignment due: ' . gmdate('d M', $course_assignment->duedate);
          } else {
            return 'No assigments due.';
          }
        }
      }
    }

   public function master_start_assignments_tabs() {
      global $CFG;
      global $DB;
      global $USER;
      global $OUTPUT;

      $main_output = '';
      $enrolled_courses = enrol_get_users_courses($USER->id, true);

      $main_output.= '<ul class="nav nav-tabs" id="myTab" role="tablist">';
      $list_count = 0;
      foreach($enrolled_courses as $enrolled_course) {
        $can_make_tab = 0;
        $activities = get_array_of_activities($enrolled_course->id);
        foreach($activities as $activity) {
          if($activity->mod == 'assign'){
            $can_make_tab = 1;
          }
        }

        if($can_make_tab == 1) {
          if($list_count == 0) {
            $main_output.= '
                <li class="nav-item">
                <a class="nav-link active"  id="course_'  . $enrolled_course->id . '-tab" data-toggle="tab" href="#course_' . $enrolled_course->id . '" role="tab" aria-controls="course' . $enrolled_course->fullname . '">' . $enrolled_course->fullname . '</a>
                </li>';
          } else {
            $main_output.= '
                <li class="nav-item">
                <a class="nav-link" id="course_' . $enrolled_course->id . '-tab" data-toggle="tab" href="#course_' . $enrolled_course->id . '" role="tab" aria-controls="course_' . $enrolled_course->id . '">' . substr( $enrolled_course->fullname ,0,15) . '...</a>
                </li>';
          }
        }
        $list_count++;
       }
      $main_output.= '</ul>';
      return  $main_output;
    }

    public function master_start_assignments_tab_lists() {
      global $CFG;
      global $DB;
      global $USER;
      global $OUTPUT;
      global $PAGE;

      $output.= '
      <div class="tab-content" id="myTabContent">';

      $enrolled_courses = enrol_get_users_courses($USER->id, true);
      $item_count == 0;
      foreach($enrolled_courses as $enrolled_course) {
        $link_arrays = array();
        $activities = get_array_of_activities($enrolled_course->id);

        $assign_activities_array = array();
        $assign_id_array = array();

        $can_make_tab = 0;
        foreach($activities as $activity) {
          if($activity->mod == 'assign'){
            $assign_activities_array[$activity->id] = $activity->name;
            $assign_id_array[$activity->id] = $activity->cm;
            $can_make_tab = 1;
          }
        }

        if($can_make_tab == 1) {
        if($item_count == 0){ 
           $output.='<div class="tab-pane fade show active" id="course_' . $enrolled_course->id  . '" role="tabpanel" aria-labelledby="course_' . $enrolled_course->id . '-tab">
          <div class="container-fluid">';
          $item_count++;
        } else {
          $output.='<div class="tab-pane fade" id="course_' . $enrolled_course->id  . '" role="tabpanel" aria-labelledby="course_' . $enrolled_course->id . '-tab">';
        }

      
          foreach($assign_activities_array as $assign_activities_id => $assign_activite_name) {
            $assignment_uploaded_sql = "SELECT 
            " . $CFG->prefix . "assign_submission.id,
            " . $CFG->prefix . "assign_submission.timecreated,
            " . $CFG->prefix . "assign_submission.status
            FROM " . $CFG->prefix . "assign_submission
            WHERE " . $CFG->prefix . "assign_submission.userid = " . $USER->id . " AND " . $CFG->prefix . "assign_submission.assignment = " . $assign_activities_id . "
            AND " . $CFG->prefix . "assign_submission.status = 'submitted'";

            $assignment_upload_results = $DB->get_record_sql($assignment_uploaded_sql, array(1));

            $locked_sql = "
                SELECT 
                  " . $CFG->prefix . "assign.allowsubmissionsfromdate
                FROM
                " . $CFG->prefix . "assign
                WHERE  " . $CFG->prefix . "assign.id = " . $assign_activities_id;

            $locked_results = $DB->get_record_sql($locked_sql, array(1));

            if (date('Y-m-d H:i:s') < date('Y-m-d H:i:s', $locked_results->allowsubmissionsfromdate)) {
              $locked = 'locked';
            } else {
              $locked = '';
            }

            //ONLY 1 RECORD SHOULD BE RETURNED SO WE DONT NEED TO LOOP
            if($assignment_upload_results->timecreated !='' && $assignment_upload_results->status == 'submitted') {
              
              //SUBMITTED
              $output .= '<div class="row assignment-wrapper align-items-center complete">
                            <div class="col-12 col-md-7">
                              <div class="row">
                                <div class="col-12 col-md-8">
                                  <h6 class="assignment-title">' . $assign_activite_name . '</h6>
                                  <p class="">
                                  </p>
                                </div>
                                <div class="col-12 col-md-4">
                                  <p class="upload-data"><img src="/theme/masterstart/images/calendar.svg" width="14" class="mr-1">Uploaded on <br/>' . date('d M h:i A', $assignment_upload_results->timecreated) .  '</p>
                                </div>
                              </div>
                            </div>';

              //NOW WE CHECK TO SEE IF THE USERS TEACHER HAS UPLOADED A REPLY
              $graded_replys_sql ="
                SELECT
                " . $CFG->prefix . "files.itemid,
                " . $CFG->prefix . "files.contextid,
                " . $CFG->prefix . "files.filename
                FROM " . $CFG->prefix . "assign_grades
                INNER JOIN " . $CFG->prefix . "files ON " . $CFG->prefix . "assign_grades.id = " . $CFG->prefix . "files.itemid
                WHERE " . $CFG->prefix . "assign_grades.userid = " . $USER->id . "
                AND " . $CFG->prefix . "assign_grades.assignment = " .  $assign_activities_id . "
                AND " . $CFG->prefix . "files.component = 'assignfeedback_file'
                AND " . $CFG->prefix . "files.filename <> '.'";

              $graded_reply = $DB->get_record_sql($graded_replys_sql);

              //GRADED PAPERS FOUND -FEEDBACK
              if(!empty($graded_reply)){
                $output .= '<div class="col-12 col-md-5">
                              <div class="row">
                                <div class="col-10 text-right">
                                  <a class="btn btn-outline-primary" href="' . $CFG->wwwroot  . '/pluginfile.php/' . $graded_reply->contextid . '/assignfeedback_file/feedback_files/' . $graded_reply->itemid . '/'. $graded_reply->filename .'?forcedownload=1" role="button" aria-pressed="true">Download
                                  </a>
                                </div>
                                <div class="col-1">
                                  <div class="assignment-status"></div>
                                </div>
                              </div>
                            </div>
                        </div>';
              } else {
                $output .= '<div class="col-12 col-md-5">
                              <div class="row">
                                <div class="col-10 text-right">
                                     </div>
                                <div class="col-1">
                                  <div class="assignment-status"></div>
                                </div>
                              </div>
                            </div>
                        </div>';
              }
            } else {
              //DOWNLOAD THE TEMPLATE
              $download_template_links_sql = "
                SELECT 
                  " . $CFG->prefix . "assign.id,
                  " . $CFG->prefix . "assign.duedate,
                  " . $CFG->prefix . "assign.allowsubmissionsfromdate,
                  " . $CFG->prefix . "course_modules.instance,
                  " . $CFG->prefix . "files.filename,
                  " . $CFG->prefix . "files.contextid,
                  " . $CFG->prefix . "files.itemid,
                  " . $CFG->prefix . "course_modules.deletioninprogress
                FROM
                " . $CFG->prefix . "course_modules
                JOIN " . $CFG->prefix . "context ON " . $CFG->prefix . "course_modules.id = " . $CFG->prefix . "context.instanceid
                JOIN " . $CFG->prefix . "assign ON " . $CFG->prefix . "course_modules.instance = " . $CFG->prefix . "assign.id
                JOIN " . $CFG->prefix . "files ON " . $CFG->prefix . "context.id = " . $CFG->prefix . "files.contextid
                WHERE  " . $CFG->prefix . "assign.id = " . $assign_activities_id . "
                AND " . $CFG->prefix . "files.component = 'mod_assign'
                AND " . $CFG->prefix . "files.filename <> '.'";

            //SHOULD ONLY BE 1 TEMPLATE PER ASSIGNMENT
            $download_template_links = $DB->get_record_sql($download_template_links_sql, array(1));

            if($download_template_links->deletioninprogress != 1) {
              $output .= '<div class="row assignment-wrapper ' . $locked . ' align-items-center">
                          <div class="col-12 col-md-7">
                            <div class="row">
                              <div class="col-12 col-md-8">
                                <h6 class="assignment-title">' . $assign_activite_name . '...</h6>
                                <p class="">
                                  <a href="' . $CFG->wwwroot  . '/mod/assign/view.php?id=' .  $assign_id_array[$assign_activities_id]. '">' . $assign_activite_name . '</a>
                                </p>
                              </div>
                              <div class="col-12 col-md-4">
                                <p class="upload-data">
                                  <img src="/theme/masterstart/images/calendar.svg" width="14" class="mr-1">Upload by <br/> ' . date('d M h:i A', $download_template_links->duedate) .'
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-5">
                            <div class="row">
                              <div class="col-10 text-right">';
                              if($download_template_links->filename != '') {
                                $output.= '<a class="btn btn-primary" href="' . $CFG->wwwroot  . '/pluginfile.php/' . $download_template_links->contextid . '/mod_assign/introattachment/' . $download_template_links->itemid . '/'. $download_template_links->filename .'?forcedownload=1">Download</a>&nbsp;';
                              }
                              $output.= '<a class="btn btn-primary uploader" data-host="' . $CFG->wwwoot . '" data-id="' . $assign_id_array[$assign_activities_id] . '" data-toggle="modal" data-target="#assignModal" href="#assignModal?assign_id=' . $assign_activities_id . '">Upload</a>
                              </div>
                              <div class="col-1">
                                <div class="assignment-status "></div>
                              </div>
                            </div>
                          </div>
                      </div>';
                }
            }

          }
                  
          $output.= '</div><!--end of fluid-->
            </div><!--end of section-->';
        //close off the can make tab check
        }
      }
      $output.= '</div><!--close off html -->';

      return $output;
    }

    public function master_start_assign_module_submission() {
      global $CFG;
      global $DB;
      global $USER;
      global $PAGE;
      global $COURSE;

      $html = '<iframe frameborder=
      "0"  width="800px" height="500" src="' . $CFG->wwwroot . '/mod/assign/view.php?id=8&action=editsubmission&pop=true#assign_anchor"></iframe><br /><br />';

      return $html;
    }

    public function master_start_course_module_blocks_full() {
      global $CFG;
      global $DB;
      global $USER;
      global $PAGE;
      global $COURSE;

      $modinfo = get_fast_modinfo($COURSE->id);
      $activities = get_array_of_activities($COURSE->id);

      $sections = array();
      $total = 0;
      $completed = 0;
      $visible = 0;

      foreach($activities as $activity) {
        if($activity->mod != 'label') {        
          if ($activity->section != 0) {
            $cm = $modinfo->get_cm($activity->cm);
            $section = $modinfo->get_section_info($activity->section);

            $sections[$activity->section]->availability = $modinfo->get_section_info_all()[$section->section]->availability;
            $sections[$activity->section]->name = $section->name;
            $sections[$activity->section]->course = $section->course;
            $sections[$activity->section]->id = $section->section;
            $sections[$activity->section]->visible = $section->visible;
            $sections[$activity->section]->activities[] = $activity;
            if($activity->completion == 2){
              $sections[$activity->section]->completed++;
            }
            $sections[$activity->section]->total++;
          }
        }
      }

      foreach($sections as $section) {
        if ($section->total != 0 || $section->completed != 0) {
          $percent_complete = number_format((($section->completed / $section->total) * 100), 0);
        } else {
          $percent_complete = 0;
        }
        
        $availability = json_decode($section->availability);
        $start_date = date('d M', $availability->c[0]->t);
        $start_date_full = date('Y-m-d', $availability->c[0]->t);

        if (isset($availability->c[1]->t)) {
          $end_date = date('d M', $availability->c[1]->t);
          $end_date_full = date('Y-m-d', $availability->c[1]->t);
        } else {
          $module_end_temp = strtotime($start_date_full);
          $module_end_temp = strtotime('+7 day', $module_end_temp);
          $end_date = date('d M', $module_end_temp);
          $end_date_full = date('Y-m-d', $module_end_temp);
        }

        $first_day = date("Y-m-d", strtotime('sunday last week'));  
        $last_day = date("Y-m-d", strtotime('sunday this week'));
        $today = date("Y-m-d", strtotime('today'));

        switch ($today) {
          case $today < $start_date_full:
              $class = 'locked';
              break;

          case $today >= $start_date_full && $today <= $end_date_full && $percent_complete != 100:
               $class = 'current';
               break;

          case $today >= $start_date_full && $today >= $end_date_full && $percent_complete != 100:
               $class = 'active';
               break;

          case $today >= $start_date_full && $today >= $end_date_full && $percent_complete == 100:
               $class = 'complete';
               break;

          case $today >= $start_date_full && $today <= $end_date_full && $percent_complete == 100:
               $class = 'complete current';
               break;
        }


        if ($percent_complete == 100 && $class != 'locked') {
          $modules .=  '<div class="course-button ' . $class . '">
                        <span><a href="' . $CFG->wwwroot . '/course/view.php?id=' . $section->course . '&section=' . $section->id . '">' . ($section->id - 1) . '</a></span>';
          $modules .=  '<div class="progress-bar">
                          <div class="fill" data-fill-amount="' . $percent_complete . '%"></div>
                        </div>';
        } else {
          if ($class == 'locked') {
            $modules .=  '<div class="course-button ' . $class . '">
                          <span>' . ($section->id - 1) . '</span>';
            $modules .=  '<img src="/theme/masterstart/images/lock.svg">
                          <div class="progress-bar"></div>';
          } else if ($class == 'complete') {
            $modules .=  '<div class="course-button ' . $class . '">
                          <span><a href="' . $CFG->wwwroot . '/course/view.php?id=' . $section->course . '&section=' . $section->id . '">' . ($section->id - 1) . '</a></span>';
            $modules .=  '<div class="progress-bar">
                            <div class="fill" data-fill-amount="100%"></div>
                          </div>';
          } else if ($class == 'active') {
            $modules .=  '<div class="course-button ' . $class . '">
                          <span><a href="' . $CFG->wwwroot . '/course/view.php?id=' . $section->course . '&section=' . $section->id . '">' . ($section->id - 1) . '</a></span>';
            $modules .=  '<div class="progress-bar">
                            <div class="fill" data-fill-amount="' . $percent_complete . '%"></div>
                          </div>';
          } else {
            $modules .=  '<div class="course-button ' . $class . '">
                          <span><a href="' . $CFG->wwwroot . '/course/view.php?id=' . $section->course . '&section=' . $section->id . '">' . ($section->id - 1) . '</a></span>';
            $modules .=  '<div class="progress-bar">
                            <div class="fill" data-fill-amount="' . $percent_complete . '%"></div>
                          </div>';
          }
        }
                        
        $modules .=    '<div class="date">' . $start_date . '</div>
                      </div>';
      }

      return $modules;
    }

    public function master_start_course_intro() {
      global $CFG;
      global $DB;
      global $USER;
      global $PAGE;
      global $COURSE;

      $content = '';

      $sections_sql = "SELECT * 
                      FROM " . $CFG->prefix . "course_sections 
                      WHERE course = " . $COURSE->id . "
                      AND section = 0";

      $sections_result = $DB->get_recordset_sql($sections_sql);

      foreach($sections_result as $section) {
        $content .= '<h2 class="card-title">Welcome to ' . $COURSE->fullname . '</h2>';
        $content .= $section->summary;
      }

      return $content;
    }

    public function master_start_course_modules() {
      global $CFG;
      global $DB;
      global $USER;
      global $PAGE;
      global $COURSE;

      $content = '';
      $content .= '<ul class="assignment-list course-module-list">';

      $modinfo = get_fast_modinfo($COURSE->id);
      $activities = get_array_of_activities($COURSE->id);

      $sections = array();
      $total = 0;
      $completed = 0;
      $visible = 0;
      foreach($activities as $activity) {
        if($activity->mod != 'label') {    
          if ($activity->section != 0) {
            $cm = $modinfo->get_cm($activity->cm);
            $section = $modinfo->get_section_info($activity->section);

            if(!$cm->uservisible) {
              continue;
            }

            $sections[$activity->section]->availability = $modinfo->get_section_info_all()[$section->section]->availability;
            $sections[$activity->section]->name = $section->name;
            $sections[$activity->section]->id = $section->section;
            $sections[$activity->section]->visible = $section->visible;
            $sections[$activity->section]->activities[] = $activity;
            if($activity->completion == 2){
              $sections[$activity->section]->completed++;
            }
            $sections[$activity->section]->total++;
          }
        }
      }

      foreach($sections as $section) {
        if ($section->completed == $section->total) {
          $section_complete = 'complete';
        } else {
          $section_complete = '';
        }

        $content .= '<li class="assignment-list-item ' . $section_complete . '">
                      <a href="/course/view.php?id=' . $COURSE->id . '&section=' . $section->id . '">
                        <div class="course-icon book"></div>
                        <div class="course-name">' . $section->name . '</div>
                        <div class="completion-icon ' . $section_complete . '"></div>
                      </a>
                    </li>';
      }

      $content .= '</ul>';

      return $content;
    }

    public function master_start_get_backlink() {
      global $COURSE, $DB, $CFG, $PAGE;

      $module = ($PAGE->cm->sectionnum - 1);
      $sectionnum = $PAGE->cm->sectionnum;
      if ($module < 0) {
        $module = 0;
        $sectionnum = 1;
      }

      $backlink = '<p><a href="' . '/course/view.php?id=' . $COURSE->id . '&section=' . $sectionnum . '" class="prev-module-link"><span><svg xmlns="http://www.w3.org/2000/svg" viewBox="150.5 0 300 295"><path d="M389 261.3L355.3 295 212 147.5 355.3 0 389 33.7 279.4 147.5 389 261.3z" fill="#32315E"/></svg></span>Back to module ' . $module . '</a></p>';

      return $backlink;
    }

    public function master_start_module_navigation() {
      global $COURSE, $DB, $CFG, $PAGE;

      $output = '';

      $settings = new StdClass;
      $settings->enabled = true;

      if ($CFG->branch >= 31) {
          if ($PAGE->cm->modname == 'assign') {
              if (optional_param('action', null, PARAM_ALPHA) == 'grader') {
                  return $output.'<!-- Navbuttons do not work with activity grading layout -->'.$outend;
              }
          }
      }
      $cmid = $PAGE->cm->id;
      $modinfo = get_fast_modinfo($COURSE);

      if ($CFG->version < 2011120100) {
          $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);
      } else {
          $context = context_course::instance($COURSE->id);
      }

      $sections = $DB->get_records('course_sections', array('course' => $COURSE->id), 'section', 'section,visible,summary');
      /** @var object|null $next */
      $next = null;
      /** @var object|null $prev */
      $prev = null;
      /** @var object|null $firstcourse */
      $firstcourse = null;
      /** @var object|null $firstsection */
      $firstsection = null;
      /** @var object|null $lastcourse */
      $lastcourse = null;
      /** @var object|null $lastsection */
      $lastsection = null;
      $sectionnum = -1;
      $thissection = null;
      $firstthissection = null;
      $flag = false;
      $sectionflag = false;
      $previousmod = null;
      $simple = null;
      $total = 1;
      $prev_count = 0;
      $next_count = 0;

      foreach ($modinfo->cms as $mod) {
          if ($mod->section != $PAGE->cm->section) {
            continue;
          }
          if ($mod->modname == 'label') {
            continue;
          }
          if ($mod->modname == 'scorm') {
            continue;
          }
          if (!$mod->uservisible) {
              continue;
          }
          
          if ($CFG->version >= 2012120300) { // Moodle 2.4.
              $format = course_get_format($COURSE);
              if (method_exists($format, 'get_last_section_number')) {
                  $numsections = $format->get_last_section_number();
              } else {
                  $opts = course_get_format($COURSE)->get_format_options();
                  $numsections = isset($opts['numsections']) ? $opts['numsections'] : 0;
              }
              if ($numsections && $mod->sectionnum > $numsections) {
                  break;
              }
          } else {
              if ($mod->sectionnum > $COURSE->numsections) {
                  break;
              }
          }
          
          if ($mod->sectionnum > 0 && $sectionnum != $mod->sectionnum) {
              $thissection = $sections[$mod->sectionnum];
              if ($thissection->visible || !$COURSE->hiddensections ||
                  has_capability('moodle/course:viewhiddensections', $context)
              ) {
                  $sectionnum = $mod->sectionnum;
                  $firstthissection = false;
                  if ($sectionflag) {
                      if ($flag) { // Flag means selected mod was the last in the section.
                          $lastsection = 'none';
                      } else {
                          $lastsection = $previousmod;
                      }
                      $sectionflag = false;
                  }
              } else {
                  continue;
              }
          }

          /* Book Navigation */
          // if (isset($_GET['id'])) {
          //   $book_id = $_GET['id'];
          // } else {
          //   $book_id = 0;
          // }
          // $previous_chapter_id = 0;
          // $next_chapter_id = 0;
          // $previous_chapter_name = '';
          // $next_chapter_name = '';
          // $previous_chapter_mod = 0;
          // $next_chapter_mod = 0;
          // $current_chapter_id = 0;

          // if (isset($_GET['chapterid'])) {
          //   $current_chapter_id = $_GET['chapterid'];
          // } else {
          //   $current_chapter_id = 0;
          // }

          // if ($mod->modname == 'book' && $book_id > 0) {
          //   $book_sql = "SELECT * 
          //               FROM " . $CFG->prefix . "course_modules 
          //               WHERE section = " . $mod->section . "
          //               AND id = ?
          //               AND module = 3";

          //   $book_result = $DB->get_recordset_sql($book_sql, array($book_id));

          //   foreach($book_result as $book) {
          //     $chapters_sql = "SELECT * 
          //                     FROM " . $CFG->prefix . "book_chapters 
          //                     WHERE bookid = " . $book->instance . "
          //                     ORDER BY pagenum ASC";

          //     $chapters_result = $DB->get_recordset_sql($chapters_sql);

          //     $previouschapter = false;
          //     $nextchapter = false;
          //     foreach($chapters_result as $chapter) {                
          //       if ($current_chapter_id == 0) {
          //         $current_chapter_id = $chapter->id;
          //       }

          //       if ($current_chapter_id == $chapter->id) {
          //         if ($previous_chapter) {
          //           $previous_chapter_id = $previous_chapter->id;
          //           $previous_chapter_name = $previous_chapter->title;
          //           $previous_chapter_mod = $book_id;
          //         }
          //       } else {
          //         $previous_chapter_id = 0;
          //       } 

          //       if ($current_chapter_id < $chapter->id) {
          //         $next_chapter_id = $chapter->id;
          //         $next_chapter_name = $chapter->title;
          //         $next_chapter_mod = $book_id;
          //         break;
          //       } else {
          //         $next_chapter_id = 0;
          //         continue;
          //       }

          //       $previous_chapter = $chapter;
          //     }
          //   }
          // }

          // if ($previous_chapter_id > 0) {
          //   $previousmod = (object)[
          //       'link' => new moodle_url('/mod/'.$mod->modname.'/view.php', array('id' => $previous_chapter_mod, 'chapterid' => $previous_chapter_id)),
          //       'name' => strip_tags(format_string($previous_chapter_name, true))
          //   ];
          // } else if ($next_chapter_id > 0) {
          //   $thismod = (object)[
          //       'link' => new moodle_url('/mod/'.$mod->modname.'/view.php', array('id' => $next_chapter_mod, 'chapterid' => $next_chapter_id)),
          //       'name' => strip_tags(format_string($next_chapter_name, true))
          //   ];
          // } else {
          //   $thismod = (object)[
          //       'link' => new moodle_url('/mod/'.$mod->modname.'/view.php', array('id' => $mod->id)),
          //       'name' => strip_tags(format_string($mod->name, true))
          //   ];
          // }

          /* Book Navigation */

          /* Non-book Navigation */

          $thismod = (object)[
              'link' => new moodle_url('/mod/'.$mod->modname.'/view.php', array('id' => $mod->id)),
              'name' => strip_tags(format_string($mod->name, true))
          ];

          /* Non-book Navigation */

          if ($flag) { // Current mod is the 'next' mod.
              $next = $thismod;
              $next_count = $total;
              $flag = false;
          }
          if ($cmid == $mod->id || $previous_chapter_id > 0) {
              $flag = true;
              $sectionflag = true;
              $prev = $previousmod;
              $prev_count = $total;
              $firstsection = $firstthissection;
              if (!$firstcourse) {
                  $firstcourse = 'none'; // Prevent the 'firstcourse' link if this is the first item.
              }
          }

          if (!$firstthissection) {
              $firstthissection = $thismod;
          }
          if (!$firstcourse) {
              $firstcourse = $thismod;
          }
          $previousmod = $thismod;

          $total++;
      }

      if (!$flag) { // Flag means selected mod is the last in the course.
          if (!$lastsection) {
              $lastsection = $previousmod;
          }
          $lastcourse = $previousmod;
      }
      if ($firstcourse == 'none') {
          $firstcourse = false;
      }
      if ($lastsection == 'none') {
          $lastsection = false;
      }

      if ($prev) {
          $output .= '
            <a href="' . $prev->link . '" class="course-link prev">
              <p>' . $prev->name . '<br/><small>step ' . $prev_count . ' of ' . $total . '</small></p>
            </a>
          ';
      } else {
          $output .= '
            <a href="' . new moodle_url('/course/view.php', array('id' => $COURSE->id)) . '" class="course-link prev">
              <p>Back to course<br/><small></small></p>
            </a>
          ';
      }
      if ($next) {
          $output .= '
            <a href="' . $next->link . '" class="course-link next">
              <p>' . $next->name . '<br/><small>step ' . $next_count . ' of ' . $total . '</small></p>
            </a>
          ';
      } else {
          $output .= '
            <a href="' . new moodle_url('/course/view.php', array('id' => $COURSE->id)) . '" class="course-link next">
              <p>Back to course<br/><small></small></p>
            </a>
          ';
      }

      return $output;
    }

    public function master_start_section_name() {
      global $CFG;
      global $DB;
      global $USER;
      global $PAGE;
      global $COURSE;

      $section = $_GET['section'];
      $content = '';

      $sections_sql = "SELECT * 
                      FROM " . $CFG->prefix . "course_sections 
                      WHERE course = " . $COURSE->id . "
                      AND section = ?";

      $sections_result = $DB->get_recordset_sql($sections_sql, array($section));

      foreach($sections_result as $section) {
        $content .= $section->name;
      }

      return $content;
    }

    public function master_start_announcements() {
      global $CFG;
      global $DB;
      global $USER;
      global $PAGE;
      global $COURSE;

      $activities = get_array_of_activities($_GET['id']);
      foreach($activities as $activity) {
        if( $activity->name == 'Announcements' && $activity->deletioninprogress != 1) {
          return '<div class="card-header text-right">
                    <span class="card-heading">
                      <a href="' . $CFG->wwwroot . '/mod/forum/view.php?id=' . $activity->cm . '"><img src="/theme/masterstart/images/message.svg" class="mr-1" width="14">Announcements</a>
                    </span>
                  </div>';
        }
      }
     
    }

    public function master_start_section_content() {
      global $CFG;
      global $DB;
      global $USER;
      global $PAGE;
      global $COURSE;

      $modinfo = get_fast_modinfo($COURSE);
      $cm = $modinfo->get_section_info($_GET['section']);

      $context = context_course::instance($COURSE->id);

      $content = file_rewrite_pluginfile_urls($cm->summary, 'pluginfile.php', $context->id, 'course', 'section', $cm->id);

      return $content;
    }
    public function master_start_section_activities() {
      global $CFG;
      global $DB;
      global $USER;
      global $PAGE;
      global $COURSE;

      $section = $_GET['section'];
      $content = '';

      $modinfo = get_fast_modinfo($COURSE->id);

      $link_array = array();
      $activities = get_array_of_activities($COURSE->id);

      $total = 0;
      $completed = 0;
      foreach($activities as $activity) {
        if ($activity->visible != 0 && $activity->deletioninprogress != 1) {
          if ($activity->section == $section) {
            $activity_list[] = $activity;
          }
        }
      }

      $content .= '<ul class="assignment-list course-module-list">';

      foreach($activity_list as $activity) {
        if($activity->mod == 'scorm') {
          continue;
        }

        if ($activity->completion == 2) {
          $complete = 'complete';
        } else {
          $complete = '';
        }

        if ($activity->mod == 'assign') { $icon = 'quiz'; }
        else if ($activity->mod == 'forum') { $icon = 'forum'; }
        else if ($activity->mod == 'page') { $icon = 'book'; }
        else if ($activity->mod == 'quiz') { $icon = 'quiz'; }
        else { $icon = 'book'; }

        if ($activity->mod == 'label' && $activity->mod != 'Label') {
          $content .= '<li class="assignment-list-item ' . $complete . ' remove-caret">
                      <a href="#">
                        <div class="course-name" style="margin: 0;">' . $activity->name . '</div>
                      </a>
                    </li>';
        } else {
          $content .= '<li class="assignment-list-item ' . $complete . '">
                        <a href="/mod/' . $activity->mod . '/view.php?id=' . $activity->cm . '">
                          <div class="course-icon ' . $icon . '"></div>
                          <div class="course-name">' . $activity->name . '</div>
                          <div class="completion-icon ' . $complete . '"></div>
                        </a>
                      </li>';
        }
      }

      $content .= '</ul>';

      return $content;
    }
       
    /**
     * We want to show the custom menus as a list of links in the footer on small screens.
     * Just return the menu object exported so we can render it differently.
     */
    public function custom_menu_flat() {
        global $CFG;
        $custommenuitems = '';

        if (empty($custommenuitems) && !empty($CFG->custommenuitems)) {
            $custommenuitems = $CFG->custommenuitems;
        }
        $custommenu = new custom_menu($custommenuitems, current_language());
        $langs = get_string_manager()->get_list_of_translations();
        $haslangmenu = $this->lang_menu() != '';

        if ($haslangmenu) {
            $strlang = get_string('language');
            $currentlang = current_language();
            if (isset($langs[$currentlang])) {
                $currentlang = $langs[$currentlang];
            } else {
                $currentlang = $strlang;
            }
            $this->language = $custommenu->add($currentlang, new moodle_url('#'), $strlang, 10000);
            foreach ($langs as $langtype => $langname) {
                $this->language->add($langname, new moodle_url($this->page->url, array('lang' => $langtype)), $langname);
            }
        }

        return $custommenu->export_for_template($this);
    }

    /*
     * This renders the bootstrap top menu.
     *
     * This renderer is needed to enable the Bootstrap style navigation.
     */
    protected function render_custom_menu(custom_menu $menu) {
        global $CFG;

        $langs = get_string_manager()->get_list_of_translations();
        $haslangmenu = $this->lang_menu() != '';

        if (!$menu->has_children() && !$haslangmenu) {
            return '';
        }

        if ($haslangmenu) {
            $strlang = get_string('language');
            $currentlang = current_language();
            if (isset($langs[$currentlang])) {
                $currentlang = $langs[$currentlang];
            } else {
                $currentlang = $strlang;
            }
            $this->language = $menu->add($currentlang, new moodle_url('#'), $strlang, 10000);
            foreach ($langs as $langtype => $langname) {
                $this->language->add($langname, new moodle_url($this->page->url, array('lang' => $langtype)), $langname);
            }
        }

        $content = '';
        foreach ($menu->get_children() as $item) {
            $context = $item->export_for_template($this);
            $content .= $this->render_from_template('core/custom_menu_item', $context);
        }

        return $content;
    }

    /**
     * This code renders the navbar button to control the display of the custom menu
     * on smaller screens.
     *
     * Do not display the button if the menu is empty.
     *
     * @return string HTML fragment
     */
    public function navbar_button() {
        global $CFG;

        if (empty($CFG->custommenuitems) && $this->lang_menu() == '') {
            return '';
        }

        $iconbar = html_writer::tag('span', '', array('class' => 'icon-bar'));
        $button = html_writer::tag('a', $iconbar . "\n" . $iconbar. "\n" . $iconbar, array(
            'class'       => 'btn btn-navbar',
            'data-toggle' => 'collapse',
            'data-target' => '.nav-collapse'
        ));
        return $button;
    }

    /**
     * Renders tabtree
     *
     * @param tabtree $tabtree
     * @return string
     */
    protected function render_tabtree(tabtree $tabtree) {
        if (empty($tabtree->subtree)) {
            return '';
        }
        $data = $tabtree->export_for_template($this);
        return $this->render_from_template('core/tabtree', $data);
    }

    /**
     * Renders tabobject (part of tabtree)
     *
     * This function is called from {@link core_renderer::render_tabtree()}
     * and also it calls itself when printing the $tabobject subtree recursively.
     *
     * @param tabobject $tabobject
     * @return string HTML fragment
     */
    protected function render_tabobject(tabobject $tab) {
        throw new coding_exception('Tab objects should not be directly rendered.');
    }

    /**
     * Prints a nice side block with an optional header.
     *
     * @param block_contents $bc HTML for the content
     * @param string $region the region the block is appearing in.
     * @return string the HTML to be output.
     */
    public function block(block_contents $bc, $region) {
        $bc = clone($bc); // Avoid messing up the object passed in.
        if (empty($bc->blockinstanceid) || !strip_tags($bc->title)) {
            $bc->collapsible = block_contents::NOT_HIDEABLE;
        }

        $id = !empty($bc->attributes['id']) ? $bc->attributes['id'] : uniqid('block-');
        $context = new stdClass();
        $context->skipid = $bc->skipid;
        $context->blockinstanceid = $bc->blockinstanceid;
        $context->dockable = $bc->dockable;
        $context->id = $id;
        $context->hidden = $bc->collapsible == block_contents::HIDDEN;
        $context->skiptitle = strip_tags($bc->title);
        $context->showskiplink = !empty($context->skiptitle);
        $context->arialabel = $bc->arialabel;
        $context->ariarole = !empty($bc->attributes['role']) ? $bc->attributes['role'] : 'complementary';
        $context->type = $bc->attributes['data-block'];
        $context->title = $bc->title;
        $context->content = $bc->content;
        $context->annotation = $bc->annotation;
        $context->footer = $bc->footer;
        $context->hascontrols = !empty($bc->controls);
        if ($context->hascontrols) {
            $context->controls = $this->block_controls($bc->controls, $id);
        }

        return $this->render_from_template('core/block', $context);
    }

    /**
     * Returns the CSS classes to apply to the body tag.
     *
     * @since Moodle 2.5.1 2.6
     * @param array $additionalclasses Any additional classes to apply.
     * @return string
     */
    public function body_css_classes(array $additionalclasses = array()) {
        return $this->page->bodyclasses . ' ' . implode(' ', $additionalclasses);
    }

    /**
     * Renders preferences groups.
     *
     * @param  preferences_groups $renderable The renderable
     * @return string The output.
     */
    public function render_preferences_groups(preferences_groups $renderable) {
        return $this->render_from_template('core/preferences_groups', $renderable);
    }

    /**
     * Renders an action menu component.
     *
     * @param action_menu $menu
     * @return string HTML
     */
    public function render_action_menu(action_menu $menu) {

        // We don't want the class icon there!
        foreach ($menu->get_secondary_actions() as $action) {
            if ($action instanceof \action_menu_link && $action->has_class('icon')) {
                $action->attributes['class'] = preg_replace('/(^|\s+)icon(\s+|$)/i', '', $action->attributes['class']);
            }
        }

        if ($menu->is_empty()) {
            return '';
        }
        $context = $menu->export_for_template($this);

        // We do not want the icon with the caret, the caret is added by Bootstrap.
        if (empty($context->primary->menutrigger)) {
            $newurl = $this->pix_url('t/edit', 'moodle');
            $context->primary->icon['attributes'] = array_reduce($context->primary->icon['attributes'],
                function($carry, $item) use ($newurl) {
                    if ($item['name'] === 'src') {
                        $item['value'] = $newurl->out(false);
                    }
                    $carry[] = $item;
                    return $carry;
                }, []
            );
        }

        return $this->render_from_template('core/action_menu', $context);
    }

    /**
     * Implementation of user image rendering.
     *
     * @param help_icon $helpicon A help icon instance
     * @return string HTML fragment
     */
    protected function render_help_icon(help_icon $helpicon) {
        $context = $helpicon->export_for_template($this);
        return $this->render_from_template('core/help_icon', $context);
    }

    /**
     * Renders a single button widget.
     *
     * This will return HTML to display a form containing a single button.
     *
     * @param single_button $button
     * @return string HTML fragment
     */
    protected function render_single_button(single_button $button) {
        return $this->render_from_template('core/single_button', $button->export_for_template($this));
    }

    /**
     * Renders a single select.
     *
     * @param single_select $select The object.
     * @return string HTML
     */
    protected function render_single_select(single_select $select) {
        return $this->render_from_template('core/single_select', $select->export_for_template($this));
    }

    /**
     * Renders a paging bar.
     *
     * @param paging_bar $pagingbar The object.
     * @return string HTML
     */
    protected function render_paging_bar(paging_bar $pagingbar) {
        // Any more than 10 is not usable and causes wierd wrapping of the pagination in this theme.
        $pagingbar->maxdisplay = 10;
        return $this->render_from_template('core/paging_bar', $pagingbar->export_for_template($this));
    }

    /**
     * Renders a url select.
     *
     * @param url_select $select The object.
     * @return string HTML
     */
    protected function render_url_select(url_select $select) {
        return $this->render_from_template('core/url_select', $select->export_for_template($this));
    }

    /**
     * Renders a pix_icon widget and returns the HTML to display it.
     *
     * @param pix_icon $icon
     * @return string HTML fragment
     */
    protected function render_pix_icon(pix_icon $icon) {
        $data = $icon->export_for_template($this);
        foreach ($data['attributes'] as $key => $item) {
            $name = $item['name'];
            $value = $item['value'];
            if ($name == 'class') {
                $data['extraclasses'] = $value;
                unset($data['attributes'][$key]);
                $data['attributes'] = array_values($data['attributes']);
                break;
            }
        }
        return $this->render_from_template('core/pix_icon', $data);
    }

    /**
     * Renders the login form.
     *
     * @param \core_auth\output\login $form The renderable.
     * @return string
     */
    public function render_login(\core_auth\output\login $form) {
        global $SITE;

        $context = $form->export_for_template($this);

        // Override because rendering is not supported in template yet.
        $context->cookieshelpiconformatted = $this->help_icon('cookiesenabled');
        $context->errorformatted = $this->error_text($context->error);
        $url = $this->get_logo_url();
        if ($url) {
            $url = $url->out(false);
        }
        $context->logourl = $url;
        $context->sitename = format_string($SITE->fullname, true, ['context' => context_course::instance(SITEID), "escape" => false]);

        return $this->render_from_template('core/login', $context);
    }

    /**
     * Render the login signup form into a nice template for the theme.
     *
     * @param mform $form
     * @return string
     */
    public function render_login_signup_form($form) {
        global $SITE;

        $context = $form->export_for_template($this);
        $url = $this->get_logo_url();
        if ($url) {
            $url = $url->out(false);
        }
        $context['logourl'] = $url;
        $context['sitename'] = format_string($SITE->fullname, true, ['context' => context_course::instance(SITEID), "escape" => false]);

        return $this->render_from_template('core/signup_form_layout', $context);
    }

    /**
     * This is an optional menu that can be added to a layout by a theme. It contains the
     * menu for the course administration, only on the course main page.
     *
     * @return string
     */
    public function context_header_settings_menu() {
        $context = $this->page->context;
        $menu = new action_menu();

        $items = $this->page->navbar->get_items();
        $currentnode = end($items);

        $showcoursemenu = false;
        $showfrontpagemenu = false;
        $showusermenu = false;

        // We are on the course home page.
        if (($context->contextlevel == CONTEXT_COURSE) &&
                !empty($currentnode) &&
                ($currentnode->type == navigation_node::TYPE_COURSE || $currentnode->type == navigation_node::TYPE_SECTION)) {
            $showcoursemenu = true;
        }

        $courseformat = course_get_format($this->page->course);
        // This is a single activity course format, always show the course menu on the activity main page.
        if ($context->contextlevel == CONTEXT_MODULE &&
                !$courseformat->has_view_page()) {

            $this->page->navigation->initialise();
            $activenode = $this->page->navigation->find_active_node();
            // If the settings menu has been forced then show the menu.
            if ($this->page->is_settings_menu_forced()) {
                $showcoursemenu = true;
            } else if (!empty($activenode) && ($activenode->type == navigation_node::TYPE_ACTIVITY ||
                    $activenode->type == navigation_node::TYPE_RESOURCE)) {

                // We only want to show the menu on the first page of the activity. This means
                // the breadcrumb has no additional nodes.
                if ($currentnode && ($currentnode->key == $activenode->key && $currentnode->type == $activenode->type)) {
                    $showcoursemenu = true;
                }
            }
        }

        // This is the site front page.
        if ($context->contextlevel == CONTEXT_COURSE &&
                !empty($currentnode) &&
                $currentnode->key === 'home') {
            $showfrontpagemenu = true;
        }

        // This is the user profile page.
        if ($context->contextlevel == CONTEXT_USER &&
                !empty($currentnode) &&
                ($currentnode->key === 'myprofile')) {
            $showusermenu = true;
        }


        if ($showfrontpagemenu) {
            $settingsnode = $this->page->settingsnav->find('frontpage', navigation_node::TYPE_SETTING);
            if ($settingsnode) {
                // Build an action menu based on the visible nodes from this navigation tree.
                $skipped = $this->build_action_menu_from_navigation($menu, $settingsnode, false, true);

                // We only add a list to the full settings menu if we didn't include every node in the short menu.
                if ($skipped) {
                    $text = get_string('morenavigationlinks');
                    $url = new moodle_url('/course/admin.php', array('courseid' => $this->page->course->id));
                    $link = new action_link($url, $text, null, null, new pix_icon('t/edit', $text));
                    $menu->add_secondary_action($link);
                }
            }
        } else if ($showcoursemenu) {
            $settingsnode = $this->page->settingsnav->find('courseadmin', navigation_node::TYPE_COURSE);
            if ($settingsnode) {
                // Build an action menu based on the visible nodes from this navigation tree.
                $skipped = $this->build_action_menu_from_navigation($menu, $settingsnode, false, true);

                // We only add a list to the full settings menu if we didn't include every node in the short menu.
                if ($skipped) {
                    $text = get_string('morenavigationlinks');
                    $url = new moodle_url('/course/admin.php', array('courseid' => $this->page->course->id));
                    $link = new action_link($url, $text, null, null, new pix_icon('t/edit', $text));
                    $menu->add_secondary_action($link);
                }
            }
        } else if ($showusermenu) {
            // Get the course admin node from the settings navigation.
            $settingsnode = $this->page->settingsnav->find('useraccount', navigation_node::TYPE_CONTAINER);
            if ($settingsnode) {
                // Build an action menu based on the visible nodes from this navigation tree.
                $this->build_action_menu_from_navigation($menu, $settingsnode);
            }
        }

        return $this->render($menu);
    }

    /**
     * This is an optional menu that can be added to a layout by a theme. It contains the
     * menu for the most specific thing from the settings block. E.g. Module administration.
     *
     * @return string
     */
    public function region_main_settings_menu() {
        $context = $this->page->context;
        $menu = new action_menu();

        if ($context->contextlevel == CONTEXT_MODULE) {

            $this->page->navigation->initialise();
            $node = $this->page->navigation->find_active_node();
            $buildmenu = false;
            // If the settings menu has been forced then show the menu.
            if ($this->page->is_settings_menu_forced()) {
                $buildmenu = true;
            } else if (!empty($node) && ($node->type == navigation_node::TYPE_ACTIVITY ||
                    $node->type == navigation_node::TYPE_RESOURCE)) {

                $items = $this->page->navbar->get_items();
                $navbarnode = end($items);
                // We only want to show the menu on the first page of the activity. This means
                // the breadcrumb has no additional nodes.
                if ($navbarnode && ($navbarnode->key === $node->key && $navbarnode->type == $node->type)) {
                    $buildmenu = true;
                }
            }
            if ($buildmenu) {
                // Get the course admin node from the settings navigation.
                $node = $this->page->settingsnav->find('modulesettings', navigation_node::TYPE_SETTING);
                if ($node) {
                    // Build an action menu based on the visible nodes from this navigation tree.
                    $this->build_action_menu_from_navigation($menu, $node);
                }
            }

        } else if ($context->contextlevel == CONTEXT_COURSECAT) {
            // For course category context, show category settings menu, if we're on the course category page.
            if ($this->page->pagetype === 'course-index-category') {
                $node = $this->page->settingsnav->find('categorysettings', navigation_node::TYPE_CONTAINER);
                if ($node) {
                    // Build an action menu based on the visible nodes from this navigation tree.
                    $this->build_action_menu_from_navigation($menu, $node);
                }
            }

        } else {
            $items = $this->page->navbar->get_items();
            $navbarnode = end($items);

            if ($navbarnode && ($navbarnode->key === 'participants')) {
                $node = $this->page->settingsnav->find('users', navigation_node::TYPE_CONTAINER);
                if ($node) {
                    // Build an action menu based on the visible nodes from this navigation tree.
                    $this->build_action_menu_from_navigation($menu, $node);
                }

            }
        }
        return $this->render($menu);
    }

    /**
     * Take a node in the nav tree and make an action menu out of it.
     * The links are injected in the action menu.
     *
     * @param action_menu $menu
     * @param navigation_node $node
     * @param boolean $indent
     * @param boolean $onlytopleafnodes
     * @return boolean nodesskipped - True if nodes were skipped in building the menu
     */
    protected function build_action_menu_from_navigation(action_menu $menu,
                                                       navigation_node $node,
                                                       $indent = false,
                                                       $onlytopleafnodes = false) {
        $skipped = false;
        // Build an action menu based on the visible nodes from this navigation tree.
        foreach ($node->children as $menuitem) {
            if ($menuitem->display) {
                if ($onlytopleafnodes && $menuitem->children->count()) {
                    $skipped = true;
                    continue;
                }
                if ($menuitem->action) {
                    if ($menuitem->action instanceof action_link) {
                        $link = $menuitem->action;
                        // Give preference to setting icon over action icon.
                        if (!empty($menuitem->icon)) {
                            $link->icon = $menuitem->icon;
                        }
                    } else {
                        $link = new action_link($menuitem->action, $menuitem->text, null, null, $menuitem->icon);
                    }
                } else {
                    if ($onlytopleafnodes) {
                        $skipped = true;
                        continue;
                    }
                    $link = new action_link(new moodle_url('#'), $menuitem->text, null, ['disabled' => true], $menuitem->icon);
                }
                if ($indent) {
                    $link->add_class('m-l-1');
                }
                if (!empty($menuitem->classes)) {
                    $link->add_class(implode(" ", $menuitem->classes));
                }

                $menu->add_secondary_action($link);
                $skipped = $skipped || $this->build_action_menu_from_navigation($menu, $menuitem, true);
            }
        }
        return $skipped;
    }

    /**
     * Secure login info.
     *
     * @return string
     */
    public function secure_login_info() {
        return $this->login_info(false);
    }
}
