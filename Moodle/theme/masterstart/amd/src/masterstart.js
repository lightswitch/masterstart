// Put this file in path/to/plugin/amd/src
// You can call it anything you like
 
define(['jquery'], function($) {
 
    return {
        init: function() {
 
            $('body').on('mouseenter mouseleave','.dropdown',function(e){
              var _d=$(e.target).closest('.dropdown');_d.addClass('show');
              setTimeout(function(){
                _d[_d.is(':hover')?'addClass':'removeClass']('show');
              },300);
            });

            $(".navbar-toggler").on('click', function(){
              $(this).toggleClass('open');
              if($(this).hasClass('main-toggle')) {
                $('#accountDropdown').removeClass('open');
                $('#accountDropdown').parent().find('.dropdown-menu').hide()
              }
            });

            $('.navbar .account-menu .navbar-nav .nav-link.dropdown-toggle').on('click', function(){
              /* Close Main Menu if Open */
              $(".navbar-toggler.main-toggle").removeClass('open');
              $('.navbar .navbar-collapse').removeClass('show');

              if ($(this).hasClass('open')) {
                $(this).removeClass('open');
                $(this).parent().find('.dropdown-menu').hide();
              } else {
                $(this).addClass('open');
                $(this).parent().find('.dropdown-menu').show();
              }
            });

            function setHeight() {
              var height = $("nav.navbar").height() + $("nav.breadcrumb-wrapper").height();
              $("body").css('padding-top', height);
            }

            $(document).ready(function(){
              setHeight();

              $(".progress-bar .fill").each(function(){
                var fillPercent = $(this).data('fill-amount');
                $(this).css('width', fillPercent);
              });

              $(".intro-content-wrapper.seen .intro-content-toggle").on('click', function(){
                $(this).parent().find('.intro-content').slideToggle();
                if($(this).hasClass('open')) {
                  $(this).removeClass('open');
                } else {
                  $(this).addClass('open');
                }
              });   

            });

            $(window).on('resize', function(){
              setHeight();
            });

            $(".navimages").find("a").each(function(){
              $(this).addClass("btn btn-primary");
              if(this.classList.contains('bookprev')) {
                $(this).addClass('previous float-left');
                $(this).text('Previous');
              } else if (this.classList.contains('booknext')) {
                $(this).addClass('next float-right');
                $(this).text('Next');
              } else if (this.classList.contains('bookexit')) {
                $(this).addClass('d-none');
                $(this).text('Finish');
              }
            });
        }
    };
});