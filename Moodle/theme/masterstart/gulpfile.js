var gulp = require('gulp');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

/*
* Scripts Task
* Uglifies
*/
gulp.task('scripts', function() {
  var scripts = [
	  'node_modules/jquery/dist/jquery.js',
	  // 'node_modules/popper.js/dist/umd/popper.js',
		// 'node_modules/bootstrap/dist/js/bootstrap.min.js',

		// 'node_modules/bootstrap/js/dist/alert.js',
		// 'node_modules/bootstrap/js/dist/button.js',
		// 'node_modules/bootstrap/js/dist/carousel.js',
		// 'node_modules/bootstrap/js/dist/collapse.js',
		// 'node_modules/bootstrap/js/dist/dropdown.js',
		// 'node_modules/bootstrap/js/dist/index.js',
		 'node_modules/bootstrap/js/dist/modal.js',
		// 'node_modules/bootstrap/js/dist/popover.js',
		// 'node_modules/bootstrap/js/dist/scrollspy.js',
		'node_modules/bootstrap/js/dist/tab.js',
		// 'node_modules/bootstrap/js/dist/tooltip.js',
		'node_modules/bootstrap/js/dist/util.js',

		'js/masterstart.js',
  ];
	gulp.src(scripts)
		.pipe(sourcemaps.init())
		.pipe(concat('masterstart.min.js'))
	  .pipe(uglify().on('error', function(e){
			console.log(e);
		}))
		.pipe(sourcemaps.write('maps'))
	  .pipe(gulp.dest('dist'));
});

gulp.task('styles', function() {
	gulp.src('scss/masterstart/*.scss')
	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
	.pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
	gulp.watch('js/*.js', ['scripts']);
	gulp.watch('scss/masterstart/*.scss', ['styles']);
});

gulp.task('default', ['scripts', 'styles', 'watch']);