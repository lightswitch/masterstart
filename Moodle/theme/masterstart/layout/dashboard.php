<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A two column layout for the boost theme.
 *
 * @package   theme_masterstart
 * @copyright 2016 Damyon Wiese
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
require_once($CFG->libdir . '/behat/lib.php');

// if (isloggedin()) {
//     $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
// } else {
    $navdraweropen = false;
// }
$extraclasses = [];
// if ($navdraweropen) {
//     $extraclasses[] = 'drawer-open-left';
// }

if (isloggedin()) {
    $loggedin = true;
    $fullname = $USER->firstname . ' ' . $USER->lastname;
    $email = $USER->email;
} else {
    $loggedin = false;
    $fullname = '';
    $email = '';
}


$coursecontext = get_context_instance(CONTEXT_COURSE, $COURSE->id);
if(has_capability('moodle/site:config', $coursecontext)) {
    $isadmin = true;
} else {
    $isadmin = false;
}

if ($isadmin || !$loggedin) {
    $isadmin = true;
} else {
    $isadmin = false;
}

$bodyattributes = $OUTPUT->body_attributes($extraclasses);
$blockshtml = $OUTPUT->blocks('side-pre');
$blockcalendar =  $OUTPUT->blocks('calendar');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;
$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();

if(isset($_GET['page']) && $_GET['page'] == 'assignment') {
    $templatecontext = [
        'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
        'pagetitle' => 'Assignments',
        'output' => $OUTPUT,
        'sidepreblocks' => $blockshtml,
        'blockcalendar' => $blockcalendar,
        'hasblocks' => $hasblocks,
        'bodyattributes' => $bodyattributes,
        'navdraweropen' => $navdraweropen,
        'regionmainsettingsmenu' => $regionmainsettingsmenu,
        'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
        'loggedin' => $loggedin,
        'fullname' => $fullname,
        'email' => $email,
        'iscourse' => $iscourse,
        'cm_name' => $cm_name,
        'module_num' => $module_num
    ];

    $templatecontext['flatnavigation'] = $PAGE->flatnav;
    $PAGE->requires->jquery();
    echo $OUTPUT->render_from_template('theme_masterstart/assignments', $templatecontext);
} else {

    // If a course
    if ($PAGE->course->category > 0) {
        $iscourse = true;
        // If a course module
        if ($PAGE->cm) {
            $cm_name = $PAGE->cm->name;
            $module_num = 'MODULE ' . $PAGE->cm->sectionnum;
        } else {
            $cm_name = $PAGE->course->fullname;
            $module_num = '';
        }
    } else {
        $iscourse = false;
        $cm_name = $PAGE->course->fullname;
        $module_num = '';
    }

    if ($PAGE->title == 'Dashboard') {
        $isdashboard = true;
    } else {
        $isdashboard = false;
    }

    $templatecontext = [
        'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
        'output' => $OUTPUT,
        'sidepreblocks' => $blockshtml,
        'blockcalendar' => $blockcalendar,
        'hasblocks' => $hasblocks,
        'bodyattributes' => $bodyattributes,
        'navdraweropen' => $navdraweropen,
        'regionmainsettingsmenu' => $regionmainsettingsmenu,
        'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
        'loggedin' => $loggedin,
        'fullname' => $fullname,
        'email' => $email,
        'isadmin' => $isadmin,
        'isdashboard' => $isdashboard,
        'iscourse' => $iscourse,
        'cm_name' => $cm_name,
        'module_num' => $module_num
    ];

    $templatecontext['flatnavigation'] = $PAGE->flatnav;
    echo $OUTPUT->render_from_template('theme_masterstart/dashboard', $templatecontext);
}