Login details
  Administrator
    admin
    Ligh_er54*I

  User
    aidan
    LS!@34aidan

Still to do:
  Header picture of course and collapsible Introduction
  Quizzes, as it stands this will require a rebuild of the standard Moodle quiz system to match designs.

Done:
  Profile picture
  Assignments card
  My assignments page
  Previous and Next buttons when in a course module/section
  Course Module status indicator
  Module activity status indicator
  Clean up Calendar on dashboard
  My Course menu status indicator and fix broken functionality
  Breadcrumbs

Questions
  What should the "Login" button do? On the designs it opens up a Modal with a button.